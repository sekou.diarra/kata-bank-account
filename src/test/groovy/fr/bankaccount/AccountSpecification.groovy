package fr.bankaccount

import fr.bankaccount.domain.Account
import fr.bankaccount.domain.Balance
import fr.bankaccount.service.AccountService
import fr.bankaccount.service.Operation
import fr.bankaccount.service.OperationRecordService
import spock.lang.Specification

import java.time.LocalDate

class AccountSpecification extends Specification {

    def "when i open a count, i should have the correct balance and the date of opening should be today"() {
        def timeOfTheDay = LocalDate.now()

        given: " A accountService"
        def accountService = new AccountService()

        when: "I open a account"
        def account = accountService.open("NO123", 'Sekou', new Balance(BigDecimal.valueOf(200)))

        then: "The getAccountBalance should be 0"
        account.balance.amount == BigDecimal.valueOf(200)

        and:
        account.dateOfOpening.toString() == timeOfTheDay.toString()
    }

    def "when perfom a deposit operation on my account, my balance should increase"() {
        def accountService = new AccountService()

        given: "A account with a initial value of 200 "
        def account = accountService.open("No123", "superMan", new Balance(BigDecimal.valueOf(200)))

        when: "I perform a deposit operation"
        def updateAccount = accountService.deposit(account, BigDecimal.valueOf(200)).get()

        then: "My balance should increase"
        updateAccount.balance.amount == BigDecimal.valueOf(400)

    }

    def "when perfom a withdraw operation on my account, my balance should decrease"() {
        def accountService = new AccountService()

        given: "A account with a initial value of 200 "
        def account = accountService.open("No123", "superMan", new Balance(BigDecimal.valueOf(200)))

        when: "I perform a withdraw operation"
        account = accountService.withdraw(account, BigDecimal.valueOf(200))

        then: "My balance should increase"
        account.get().balance.amount == BigDecimal.ZERO

    }

    def "when i try to withdraw insuficient money on my account, the operation should fail"() {
        def accountService = new AccountService()

        given: "A account with a initial value of 200 "
        def account = accountService.open("No123", "superMan", new Balance(BigDecimal.valueOf(200)))

        when: "I perform a withdraw operation with a amount superior of my current balance"
        account = accountService.withdraw(account, BigDecimal.valueOf(201))

        then: "the operation should fail"
        account.isFailure()

    }

    def "when asking for history, i should get a details of all my opetation"() {
        def operationRecordService = new OperationRecordService()
        def accountService = new AccountService()

        given: "A account with multiple operation perform on it"
        def account = new Account(operationRecordService)
//        account.operationRecordService=operationRecordService

        account = accountService.deposit(account, BigDecimal.valueOf(1000)).get()
        account = accountService.withdraw(account, BigDecimal.valueOf(200)).get()
        account = accountService.withdraw(account, BigDecimal.valueOf(400)).get()


        when: "I ask for history"
        accountService.getHistory(account)

        then: "operations should print in this following order"
        def operation1 = new Operation(LocalDate.now(), new BigDecimal(1000), new BigDecimal(1000))
        def operation2 = new Operation(LocalDate.now(), new BigDecimal(-200), new BigDecimal(800))
        def operation3 = new Operation(LocalDate.now(), new BigDecimal(-400), new BigDecimal(400))


        def expectedOperations = [operation3, operation2, operation1]
        operationRecordService.getReversedListOperation() == expectedOperations
    }
}
