package fr.bankaccount.domain

import fr.bankaccount.service.Operation
import fr.bankaccount.service.OperationRecordService
import org.funktionale.tries.Try
import java.math.BigDecimal
import java.time.LocalDate


typealias Amount = BigDecimal

private fun today() = LocalDate.now()

class Balance(val amount: Amount = BigDecimal.ZERO)

data class Account private constructor(val no: String, val name: String, val dateOfOpening: LocalDate, val balance: Balance, val operationRecordService: OperationRecordService = OperationRecordService()) {

    constructor(operationRecordService: OperationRecordService) : this("", "", today(), Balance(), operationRecordService)

    companion object {


        fun openNewAccount(no: String, name: String, balance: Balance): Account {

            val balanceToSet: Balance = if (balance.amount < BigDecimal.ZERO) {
                Balance()
            } else {
                balance
            }
            return Account(no, name, today(), balanceToSet)
        }

        fun updateBalance(account: Account, newAmount: Amount): Try<Account> {
            return Try {
                checkBalance(account, newAmount)
                account.operationRecordService.recordOperation(Operation(today(), newAmount, (account.balance.amount + newAmount)))
                return@Try account.copy(balance = Balance(account.balance.amount + newAmount))
            }
        }

        private fun checkBalance(account: Account, amountToCheck: Amount) {
            if (amountToCheck < BigDecimal.ZERO && account.balance.amount < -amountToCheck)
                throw Error("Invalid operation!!! your account balance can not be")
        }

    }

    fun provideHistory() {
        operationRecordService.getReversedListOperation().forEach({ it -> println(it) })
    }

}
