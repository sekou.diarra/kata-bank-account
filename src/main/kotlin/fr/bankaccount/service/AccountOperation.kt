package fr.bankaccount.service

import org.funktionale.tries.Try

interface AccountOperation<Account,  Amount , Balance> {
    fun open(no: String, name: String,balance: Balance): fr.bankaccount.domain.Account
    fun withdraw(account: Account, amount: Amount): Try<fr.bankaccount.domain.Account>
    fun deposit(account: Account, amount: Amount): Try<fr.bankaccount.domain.Account>
    fun getAccountBalance(account: Account): Balance
    fun getHistory(account: Account)
}
