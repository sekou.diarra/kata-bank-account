package fr.bankaccount.service

import java.math.BigDecimal
import java.time.LocalDate
import java.time.format.DateTimeFormatter


class OperationRecordService(private val listOperations: MutableList<Operation> = mutableListOf()) {

    fun getReversedListOperation():List<Operation>{
        return listOperations.reversed()
    }

    fun recordOperation(operation: Operation) {
        listOperations.add(operation)
    }

}

data class Operation(private val date: LocalDate, private val amount: BigDecimal, private val balance: BigDecimal) {
    override fun toString(): String {
        return "${date.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))} | ${amount.setScale(2)} | ${balance.setScale(2)}"
    }
}

