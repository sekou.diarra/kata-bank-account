package fr.bankaccount.service

import fr.bankaccount.domain.Account
import fr.bankaccount.domain.Amount
import fr.bankaccount.domain.Balance
import org.funktionale.tries.Try


internal enum class OPERATION {
    WITHDRAW, DEPOSIT
}

object AccountService : AccountOperation<Account, Amount, Balance> {

    override fun open(no: String, name: String, balance: Balance): Account {
        return Account.openNewAccount(no, name, balance)
    }

    override fun withdraw(account: Account, amount: Amount): Try<Account> = performOperation(account, amount, OPERATION.WITHDRAW)

    override fun deposit(account: Account, amount: Amount): Try<Account> = performOperation(account, amount, OPERATION.DEPOSIT)

    override fun getAccountBalance(account: Account): Balance {
        return account.balance
    }

    override fun getHistory(account: Account) {
        account.provideHistory()
    }

    private fun performOperation(account: Account, amount: Amount, operation: OPERATION): Try<Account> {
        return when (operation) {
            OPERATION.WITHDRAW -> Account.updateBalance(account, -amount)
            OPERATION.DEPOSIT -> Account.updateBalance(account, amount)
        }
    }

}
